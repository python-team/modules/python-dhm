#!/usr/bin/python

from distutils.core import setup

setup(	name		= "dhm",
	version		= "0.6",
	author		= "Wichert Akkerman",
	author_email	= "wichert@deephackmode.org",
	license		= "GPL version 2",
	description	= "DeepHackMode python tools",
	packages	= [ "dhm", "dhm.sql" ],
	package_dir	= { "dhm" : "src" },
	keywords	= [ "SQL", "LDAP", "utility" ],
	)
